# TinyMap For HarmonyOS

#### 介绍
鸿蒙操作系统中，简单易用的轻量化地图组件。
Easy-to-use lightweight map component for HarmonyOS

优势：
1. 超级轻量化，目前仅包含3个类，适合源码研究和学习。
2. 可高德地图和Google地图作为地理底图，并且可以选择卫星影像和矢量底图数据。
3. 可添加自定义底图元素（Element）

#### 安装教程

将tinymap Module（也就是HAP）移到所需要的工程中，并且在需要调用的Module（HAP）中导入这个工程。
在build.gradle中插入以下代码：

```
apply plugin: 'com.huawei.ohos.hap'
...

dependencies {
    ...
    implementation project(':tinymap')
}

```


#### 使用说明

目前支持手势滑动平移，还不支持手势放大和缩小（在后期会加上这个功能）。
1.  zoomIn()方法：缩小地图
2.  zoomOut()方法：放大地图
2.  refreshMap()方法：刷新地图
3.  setMapSource(TinyMap.MapSource mapSource)方法：切换底图数据源。目前底图数据源包括5类：

> MapSource.GAODE_ROAD : 高德道路数据
> MapSource.GAODE_VECTOR : 高德矢量数据
> MapSource.GAODE_SATELLITE : 高德卫星数据
> MapSource.GOOGLE_VECTOR : Google矢量数据
> MapSource.GOOGLE_SATELLITE : Google卫星数据

5. addElement(float x, float y, int resource)方法：添加底图元素(目前仅支持墨卡托投影坐标，后期会添加经纬度坐标方法)。例如：

`addElement(12956517.35f, 4864667.87f, ResourceTable.Media_dot)`

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
